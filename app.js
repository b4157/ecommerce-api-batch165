//[SECTION] Packages and Dependencies
  const express = require("express"); 
  const mongoose = require("mongoose");  
  const dotenv = require("dotenv");  
  const orderRoutes = require('./routes/orders');
  const productsRoutes = require('./routes/products');  
  const userRoutes = require('./routes/users');
  const cors = require("cors");

//[SECTION] Server Setup
  const app = express(); 
  dotenv.config(); 
  app.use(express.json());
  const secret = process.env.CONNECTION_STRING;
  const port = process.env.PORT; 
  app.use(cors());

//[SECTION] Application Routes
  app.use('/orders', orderRoutes);
  app.use('/products', productsRoutes);
  app.use('/users', userRoutes);

//[SECTION] Database Connection
  mongoose.connect(secret)
  let connectStatus = mongoose.connection; 
  connectStatus.on('open', () => console.log('Database is Connected'));

//[SECTION] Gateway Response
  app.get('/', (req, res) => {
     res.send(`Welcome to LAGUDA GROCERY STORE'S ONLINE SHOP!`); 
  }); 

  app.listen(port, () => console.log(`Server is running on port ${port}`)); 