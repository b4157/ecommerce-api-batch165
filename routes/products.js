// [SECTION]: Dependencies and Modules
  const exp = require("express");
  const controller = require('../controllers/products.js');
  const auth = require("../auth");

  // destructure verify from auth
    const {verify, verifyAdmin} = auth;

// [SECTION]: Routing Component
  const route = exp.Router(); 

// [SECTION]: POST ROUTES (Create)
 route.post('/add-product', verify, verifyAdmin, (req,res)=>{
    let prodDetails = req.body;
  
    controller.addProd(prodDetails).then(outcome => {
      res.send(outcome);
    });

  })

// [SECTION]: GET ROUTES (Retrieve)
  // retrieve all prduct
    route.get('/product-list-dakjuk', (req, res) => {
   
      controller.getAllProducts().then(result => {
        res.send(result);
      });
   });
    
  //Retrieve single product 
    route.get('/:id', (req,res)=>{
      // console.log(req.params.id);
      let prodId = req.params.id;

      controller.getProduct(prodId).then(result =>{
        res.send(result)
      })

   })

// [SECTION]: PUT ROUTES (Update)
  // activate or deactivate product
    route.put('/:id/set-active-stat', verify, verifyAdmin, (req, res) => {
      
       let prodId = req.params.id;

       controller.setStat(prodId).then(resultofTheFunction => {
          res.send(resultofTheFunction);
       });
    })
  // update product details
    route.put('/:id',verify, verifyAdmin, (req, res) => {
      
     let id = req.params.id;
      let details = req.body;

      controller.updateProduct(id, details).then(result=>{
        res.send(result);
      })
    })
  // update product inventory count after physical count
    route.put('/update-product-inventory-count', verify, verifyAdmin, (req, res) => {
      
      let id = req.body.id;
      let details = req.body;

      controller.updateInventCount(id, details).then(result=>{
        res.send(result);
      })
    })
  // add product stocks after purchase order
    route.put('/add-product-inventory-count', verify, verifyAdmin, (req, res) => {
      
      let id = req.body.id;
      let details = req.body;

      controller.addInventCount(id, details).then(result=>{
        res.send(result);
      })
    })
  // subtract stocks due to being damage or return
    route.put('/subtract-product-inventory-count', verify, verifyAdmin, (req, res) => {
      
      let id = req.body.id;
      let details = req.body;

      controller.diffInventCount(id, details).then(result=>{
        res.send(result);
      })
    })

// [SECTION]: DELETE ROUTES (Delete)
  route.delete('/:id/delete', verify, verifyAdmin, (req, res)=>{

    let id = req.params.id;

    controller.deleteProduct(id).then(result=>{
      res.send(result);
    });
    
  });

// [SECTION]: Export Route System
  module.exports = route;