// [SECTION]: Dependencies and Modules
  const exp = require("express");
  const User = require('../models/user');
  const controller = require('../controllers/users.js');
  const auth = require("../auth");

  // destructure verify from auth
    const {verify, verifyAdmin} = auth;

// [SECTION]: Routing Component
    const route = exp.Router(); 
  
// [SECTION]: POST ROUTES (Create)
  // register new user
    route.post('/register', (req,res)=>{
      let userDetails = req.body;
    
      controller.registerUser(userDetails).then(outcome => {
        res.send(outcome);
      });
    })
  // login existing user
    route.post('/login', (req, res) => {
      let userDetails = req.body;

      controller.loginUser(userDetails)
      .then(outcome=>{
        res.send(outcome);
      });
    
    });

    // route.post('/login', controller.loginUser); 

// [SECTION]: GET ROUTES (Retrieve)
  //rerieve all users for admin only 
    route.get('/all', verify, verifyAdmin, (req, res) => {
     
        controller.getAllUsers().then(result => {
          res.send(result);
        });
     });
  // retrieve single record, admin only
    route.get('/single-user', verify, verifyAdmin, (req, res) => {
        let userId = req.body.id;

        controller.getSingleUser(userId).then(result => {
          res.send(result);
        });
     }); 

    // retrieve singleby email
    route.get('/:email', (req, res) => {
       
        let email = req.params.email;
        controller.getSingleUserbyEmail(email).then(result => {
         
          res.send(result);
        });

     });
  // retrieve own record
    route.get('/own-account', verify, (req, res) => {
        let userId = req.user.id;
        controller.getSingleUser(userId).then(result => {
          res.send(result);
        });
     });

// [SECTION]: PUT ROUTES (Update)
  // create order with async await
    
  // update own account data, logged-in 
    route.put('/update-user-info', verify, (req, res) => {
      
      let id = req.user.id;
      let details = req.body;

      console.log(id)

      controller.updateUser(id, details).then(result=>{
        res.send(result);
      })
    })
  // setting admin account
    route.put('/:id/set-admin', verify, verifyAdmin, (req, res) => {
          
          let userId = req.params.id;

          controller.setAdmin(userId).then(result =>{
            res.send(result)
          })
    })
  // de-activate or activate user's account
    route.put('/:id/set-active-stat', verify, verifyAdmin, (req, res) => {
      
       let userId = req.params.id;

       controller.activeStat(userId).then(resultofTheFunction => {
          res.send(resultofTheFunction);
       });
    })
  // Add shipping address
    route.put('/:id/add-shipping-address', verify, (req, res) => {
      let userId = req.params.id;
      let data = req.body;

      controller.addShipInfo(userId, data).then(outcome => {
        res.send(outcome);
      });
    });

// [SECTION]: DELETE ROUTES (Delete)
  // delete user's account
    route.delete('/:id/delete-user-account', verify, verifyAdmin, (req, res)=>{

      let id = req.params.id;

      controller.deleteUser(id).then(result=>{
        res.send(result);
      });
      
   });
  // delete own account
    route.delete('/delete-own-account', verify, (req, res)=>{

      let id = req.user.id; 

      controller.deleteUser(id).then(result=>{
        res.send(result);
      });
      
    });

// [SECTION]: Export Route System
  module.exports = route;