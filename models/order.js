//[SECTION] Dependencies and Modules
  const mongoose = require("mongoose"); 

//[SECTION] Schema / Document Blueprint
  const orderSchema = new mongoose.Schema({
   
    totalAmount: {
      type: Number,
      required: [true, 'Toal amount is required']
    },
    purchasedOn: {
      type: Date,
      default: Date.now,
      required: [true, 'date created is required']
    },
    
    customer: [
      {
        _id: {
          type: String,
        },
        firstName: {
          type: String,
          required: [true, 'First name is required']
        },
        lastName: {
          type: String,
          required: [true, 'Last name is required']
        }
      }
    ],
    productOrders: [
      {
        _id: {
          type: String,
          required: [true, 'Product ID is Required']
        },
        productName:{
           type: String,
          required: [true, 'Product Name is Required']
        },
        prodQty:{
          type: Number,
          required: [true, 'Product Quantity is required']
        }
        // prodPrice:{
        //   type: Number,
        //   required: [true, 'Product Price is required']
        // }
      }
    ],
    paymentChannel: {
      type: String,
    },
    isCOmplete:{
      type: String,
      default: false
    },
    shippngInfo:[
      {
        shippingId:{
          type: String,
        },
        purok: {
          type: String,
          required: [true, 'Purok is is required']
        },
        barangay: {
          type: String,
          required: [true, 'Barangay is is required']
        },
        municipality: {
          type: String,
          required: [true, 'Municipality is is required']
        },
        province: {
          type: String,
          required: [true, 'Province is is required']
        }           
      }]
  })

//[SECTION] Model
   
   const Order = mongoose.model("Order", orderSchema); 
   module.exports = Order; 