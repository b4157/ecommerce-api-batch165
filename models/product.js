//[SECTION] Dependencies and Modules
  const mongoose = require("mongoose"); 

//[SECTION] Schema / Document Blueprint
   const productSchema = new mongoose.Schema({
   
    productName: {
        type: String,
        required: [true, 'Product Name is required']
      },
    description: {
        type: String,
        required: [true, 'Password is required']
      },
    price: {
        type: Number,  
        required: [true, 'Price is required']
      },
    isActive: {
        type:  Boolean,  
        default: true
      },
    createdOn: {
        type: Date,
        default: Date.now
      },
    inventCount: {
        type: Number, 
      },
    supplier: {
        type: String,
        required: [true, 'Supplier is required']
      },
    prodCategory:{
        type: String,
        required: [true, 'Product Category is required']
    },
    cog: {
        type: Number,
        required: [true, 'Cost of good is required']
    },
    photo:{
        type: String,
        required: [true, 'Picture of the product is required'],
        unique: true
    }
  });

//[SECTION] Model
   
   const Product = mongoose.model("Product", productSchema); 
   module.exports = Product; 