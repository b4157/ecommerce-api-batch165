//[SECTION] Dependencies and Modules
  const mongoose = require("mongoose"); 

//[SECTION] Schema / Document Blueprint
  const userSchema = new mongoose.Schema({
    firstName:{
        type: String,
        required: [true, 'First Name is required']
    },
    lastName:{
        type: String,
        required: [true, 'Last Name is required']
    },
    email: {
        type: String,
        required: [true, 'Email is required'],

      },
    password: {
        type: String,
        required: [true, 'Password is required']
      },
    isAdmin: {
        type: Boolean,  
        default: false 
      },
    mobileNum: {
        type: String
    },
    shippngInfo:[
      {
        shippingId:{
          type: String,
        },
        purok: {
          type: String,
          required: [true, 'Purok is is required']
        },
        barangay: {
          type: String,
          required: [true, 'Barangay is is required']
        },
        municipality: {
          type: String,
          required: [true, 'Barangay is is required']
        },
        province: {
          type: String,
          required: [true, 'Barangay is is required']
        }           
      }
    ],
    isActive: {
        type: Boolean,
        default: true
    }
  })

//[SECTION] Model
   
   const User = mongoose.model("User", userSchema); 
   module.exports = User; 