// [SECTION]: DEPENDENCIES AND MODULES
	const auth = require("../auth")
	const User = require('../models/user');
	const Product = require('../models/product');
	const Order = require('../models/order');
	const dotenv = require("dotenv");
// [SECTION]: ENVIRONMENT SET-UP 
	dotenv.config();
   
// [SECTION]: FUNCTIONALITY - CREATE

	// user add product to cart
		module.exports.addToCart =  (id, data) => {			
			
			let cId = id;
			let prodId = data.productOrders[0]._id;
		    let prodQty = data.productOrders[0].prodQty;
		    let productName = data.productOrders[0].productName;
		   		
		
			return Order.findOne({"customer._id": cId, isCOmplete: false}).then(result => {	   
				
	        
	        	if(result !== null){

	        	    let orderId = result.id;    	
	        	    console.log(orderId)	
		       		let prevTotalAmount = result.totalAmount
		       		let newTotalAmount = prevTotalAmount + data.totalAmount
	       		
		       		return Order.findOne({"customer._id": cId, isCOmplete: false, "productOrders._id": prodId}).then(prodResult => {
		       			 
		       			if (prodResult !== null){
			       			
			       			const test =[];

			       			test.push(prodResult.productOrders)	

			       			let prodIndex = test.findIndex(x => x._id === prodId)
			       			console.log(prodIndex)

			       			let newProdQty = prodResult.productOrders[(-1*prodIndex)-1].prodQty	+ prodQty    			
			       			console.log(newProdQty)
			       			
			       			let prodUp = {
			       				totalAmount:newTotalAmount,
					        	$set: {"productOrders.$.prodQty": newProdQty}

		        		 	}

		        		 	return Order.findOneAndUpdate({_id:orderId,"productOrders._id": prodId},prodUp).then((prodAdded, err) => {
								console.log(prodAdded)
				        		if (prodAdded) {
				          			return true
				        	
						        } else {	
						          return err;
					        	}
		      				});

	        		 	} else {

	        		 		let newProd = {
			       				totalAmount:newTotalAmount,
					        	$push: {
					        		productOrders:[{
										_id: prodId,
										productName:productName,
										prodQty: prodQty

					        		}]
					        	}

		        		 	}

		        		 	return Order.findByIdAndUpdate(orderId, newProd).then((prodAdded, err) => {
						
				        		if (prodAdded) {
				          			return  true
				        	
						        } else {	
						          return err;
					        	}

		      				});


	        		 	}
		       		})

		      	
		      	} else {

	        		return User.findById(cId).then(userResult => {
		    			
		    			 let fName = userResult.firstName
				         let lName = userResult.lastName
				         let tAmount = data.totalAmount
				         
				         let customer = [{
				         	"_id":cId, 
				         	"firstName": fName, 
				         	"lastName": lName
				         }]

				       	 let poductsIds = data.productOrders;	
						
						 let newOrder = new Order({	
							totalAmount: tAmount,
							customer: customer,
							productOrders: poductsIds
						
						 });

						 return newOrder.save().then( (orderCreated, rejected) => {
							if (orderCreated) {
								return orderCreated;
							} else {
								return false
							}	
						 });
					}); 
	        	 
				}

	     	});   
		};
	// guest checkout
		module.exports.checkoutGuest = (data) => {			
			 
	         let customer = data.customer 
	         let tAmount = data.totalAmount;
	       	 let poductsIds = data.productOrders;	
			 let pChannel = data.paymentChannel;
			 let shippngInfo = data.shippngInfo;

			 let newOrder = new Order({	
				totalAmount: tAmount,
				customer: customer,
				productOrders: poductsIds,
				paymentChannel: pChannel,
				shippngInfo: shippngInfo,
				isCOmplete: true
			 });

			
			return newOrder.save().then((orderCreated, rejected) => {
				if (orderCreated) {
					return orderCreated;
				} else {
					return 'Failed to create new account'
				}	
			});
		};

// [SECTION]: FUNCTIONALITY - RETRIEVE
	// Retrieve all orders admin only 
		module.exports.getAllOrders = () => {
	        return Order.find({}).then(result => {
	        return result;
	     });
	    };
	// Retrieve orders from authenticated users
		module.exports.getAuthenticatedOrders = (id) => {
	       
	        return Order.find({"customer._id": id}).then(result => {	   
	        	return result
	     	});
	    };
	    // Retrieve orders from authenticated users
		module.exports.getProductOrders = (id) => {
	       
	        return Order.find({"customer._id": id, isCOmplete:false}).then(result => {	   
	        	return result
	     	});
	    };// Retrieve orders from authenticated users
		module.exports.getProductOrders1 = (id) => {
	       
	        return Order.find({"productOrders._id": id, isCOmplete:false}).then(result => {	   
	        	return result
	     	});
	    };

// [SECTION]: FUNCTIONALIY - UPDATE
	// authenticated user check-out
		module.exports.authCheckout =  (userId, data) => {  
	       console.log(userId)
	        
	        return User.findById(userId).then(result => {	   
	      
	        	let shippngInfo= result.shippngInfo;
	        	console.log(shippngInfo)
	        	
	        	return Order.find({"customer._id": userId, isCOmplete:false}).then(OrderResult => {		
	         		
	         		console.log(OrderResult)
	         		
	         		if (OrderResult.length !== 0) {
	         			
	         			let orderId = OrderResult[0]._id;
						let pChannel = data.paymentChannel; 
						
						let updatedOrder = {
							paymentChannel: pChannel,
							shippngInfo: shippngInfo,
							isCOmplete: true
						}			

						return Order.findByIdAndUpdate(orderId, updatedOrder).then((checkoutSuccess, err) => {
			
			        		if (checkoutSuccess) {
			          			return true
			          			 // `Your order has been placed. Delivery will be within the day if it was placed before 4pm within the poblacion and the following day if placed after the cut-off.`
			        	
					        } else {
					          return err;
				        	}
		      			});  
	         		
	         		} else {
	         			
	         			return false 
	         			// {message:(`Order not placed. Kindly add product to cart first.`)} 
	         		}
	         	
				});        
	     	});	

	  
	      	 	
	 	}
