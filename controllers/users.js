// [SECTION]: DEPENDENCIES AND MODULES
	const auth = require("../auth")
	const User = require('../models/user');
	const Product = require('../models/product');
	const Order = require('../models/order');
	const bcrypt = require("bcrypt");  
	const dotenv = require("dotenv"); 

// [SECTION]: ENVIRONMENT SET-UP 
	dotenv.config();
	const salt = parseInt(process.env.SALT);

// [SECTION]: FUNCTIONALITY - CREATE
	// create user
		module.exports.registerUser = (data) => {
			
			let fName = data.firstName;
			let lName = data.lastName;
			let email = data.email;
			let passW = data.password;
			let mobil = data.mobileNum;

			let newUser = new User({
				
					firstName: fName,
					lastName: lName,
					email: email,
					password: bcrypt.hashSync(passW,salt),
					mobileNo: mobil
			});

			return newUser.save().then( (userCreated, rejected) => {
				if (userCreated) {
					return userCreated;
				} else {
					return ({message:'Failed to Register new account'})
				}	
			});
		};

// [SECTION]: FUNCTIONALITY - RETRIEVE
	// Login
		module.exports.loginUser = (userData) => {
			
			let email=userData.email;
			let passW=userData.password;

			return User.findOne({email:email}).then(result => {
				
				if (result === null){
					return false 
					// {message:('Email not found')}				
				// } else if (result.isActive === false){
				// 	return {message:('Account inactive. Request activate first')}
				}else {
					const isPasswordCorrect = bcrypt.compareSync(passW, result.password)
			     
			      if(isPasswordCorrect) {
			      
			          return {accessToken: auth.createAccessToken(result), email: email, isAdmin: result.isAdmin}
			      
			       } else {
			          return false
			          // {message: ("Incorrect Password")}
			       }
				}

			})
		} 


	// get all users for admin only
		module.exports.getAllUsers = () => {
	        return User.find( {} ).then(result => {
	        return result;
	     });
	   };
	// retrieve single record
		module.exports.getSingleUser = (id) => {
	        return User.findById(id).then(result => {
	        	return result;
	     	});
	  };
	// retrieve single record
		module.exports.getSingleUserbyEmail = (email) => {

			return User.findOne({email:email}).then(result => {
				console.log(email)

				if (result !== null){
					return ({auth: "Email already in use!"}); 
				}else {
					return null
				}

			})
		} 

// [SECTION]: FUNCTIONALITY - UPDATE
	//update own account 
		module.exports.updateUser = (id, details) => {
	        
	    let fName = details.firstName;
			let lName = details.lastName;
			let email = details.email;
			let passW = details.password;
			let mobil = details.mobileNum;
			

	        let updatedUser = {
	          firstName: fName,
	          lastName: lName,
	          email: email,
	          password: bcrypt.hashSync(passW,salt),
	          mobileNum: mobil
	        }

	      return User.findByIdAndUpdate(id, updatedUser).then((userUpdated, err) => {
	        
	        if (err) {
	          return ({message:`Failed to update. Error:`})
	        } else {
	          return  ({message:`ID: ${id} has been successfully updated`});
	        }

	      });
	    }
	// set up admin account, admin only
	  module.exports.setAdmin = (id) => {  

	      return User.findById(id).then(result => {		
	       	
	       	let adStat = result.isAdmin
	         let adStat1 = {
	         		isAdmin:!adStat
	         }

	         return User.findByIdAndUpdate(id, adStat1).then((adminStat, err) => {
		
		        if (adminStat) {
		          return  ({message:`The user with id: ${id} has been updated.`})
		        	
		        } else {
		          return ({message:`Failed to set user's role: ${id}.` })
		        }
	      	})    
	      }) 	
	  }
	// Archivng and activating product,admin only
		module.exports.setUserStat = (id, details) => {    
				
				return Product.findById(id).then(result => {				
					
					let update = result.isActive 
					
					let updates= {
						isActive: !update
					}

					return Product.findByIdAndUpdate(id, updates).then((adminStat, err) => {
		        
				        if (adminStat) {
				          return ({message:`The account with id: ${id} has been updated.`})
				        	
				        } else {
				          return ({message: `Failed to activate/deactivate account with id: ${id}.`})
				        }
	      		})
			})		
	 	}
	// deactivate or activate account
		module.exports.activeStat = (id) => {  

	      return User.findById(id).then(result => {		
	         let adStat = !result.isActive 
	         let adStat1 = {
	         		isActive:adStat
	         }

	         return User.findByIdAndUpdate(id, adStat1).then((adminStat, err) => {
		
		        if (adminStat) {
		        	// return res.send({auth: "Failed. No Token"});
		          return  ({message:`The user with id: ${id} has been updated.`})
		        	
		        } else {
		          return ({message:`Failed to set user's role: ${id}.`}) 
		        }
	      	})    
	      }) 	
	  }
	// add shipping information		
	  module.exports.addShipInfo = (id, data) => {  

	      return User.findById(id).then(result => {		
         			let purok = data.purok;
					let barangay = data.barangay;
					let municipality = data.municipality;
					let province = data.province;
	        
	        let newShippingAdd = {
	        	$set: {
	        		shippngInfo:[{
		        		purok: purok,
								barangay: barangay,
								municipality: municipality,
								province: province
	        		}]

	        	}
							
			};
	    	  
	    	return User.findByIdAndUpdate(id, newShippingAdd).then((adminStat, err) => {
		
		        if (adminStat) {
		          return ({message: `The user with id: ${id} has been updated.`})
		        	
		        } else {
		          return ({message:`Failed to set user's role: ${id}.`}) 
		        }
	      	})    
	      }) 	
	  }

// [SECTION]: FUNCTIONALITY - DELETE
	// delete user's account
	 	module.exports.deleteUser = (id) =>{

		    return User.findByIdAndRemove(id).then((removedUser, err)=>{
		     
		      if (removedUser) {
		        return true
		      } else {
		        return false
		      }

		    });
	   }
