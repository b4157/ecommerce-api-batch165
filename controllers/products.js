// [SECTION]: DEPENDENCIES AND MODULES
	const auth = require("../auth")
	const User = require('../models/user');
	const Product = require('../models/product');
	const Order = require('../models/order');
	const bcrypt = require("bcrypt");  
	const dotenv = require("dotenv"); 

// [SECTION]: ENVIRONMENT SET-UP 
	dotenv.config();
	const salt = parseInt(process.env.SALT);

// [SECTION]: FUNCTIONALITY - CREATE
	// Create product
		module.exports.addProd = (data) => {
			
			let pName = data.productName;
			let desc = data.description;
			let price = data.price;
			let qty = data.inventCount;
			let supplier = data.supplier;
			let category = data.prodCategory;
			let cog = data.cog
			let photo = data.photo

			let newProduct = new Product({
				
				productName: pName,
				description: desc,
				price: price,
				inventCount: qty,
				supplier: supplier,
				prodCategory: category,
				cog: cog,
				photo: photo
			});

			return newProduct.save().then( (prodCreated, rejected) => {
				if (prodCreated) {
					return prodCreated;
				} else {
					return false;
				}	
			});
		};

// [SECTION]: FUNCTIONALITY - RETRIEVE
	//View all products 
		module.exports.getAllProducts = () => {
	        return Product.find( {supplier:"Resto"} ).then(result => {
	        	return result;
	     	});
	    };
	// View a single product
	  module.exports.getProduct = (id) => {
	    return Product.findById(id).then(resultOfQuery => {
	        return resultOfQuery;
	    })
	  };

// [SECTION]: FUNCTIONALITY - UPDATE
	// Archivng and activating product
		module.exports.setStat = (id, details) => {    
				
				return Product.findById(id).then(result => {				
					
					let update = result.isActive 
					
					let updates= {
						isActive: !update
					}

					return Product.findByIdAndUpdate(id, updates).then((adminStat, err) => {
		        
				        if (adminStat) {
				          return true;
				        	
				        } else {
				          return false;
				        }
	      		})
			})		
	 	}
  // Updating Product information
		module.exports.updateProduct = (id, details) =>{
			
			let pName = details.productName;
			let desc = details.description;
			let price = details.price;
			let supp = details.supplier;
		  let prodC = details.prodCategory;
		  let cog = details.cog;
		  let photo = details.photo

		  let updatedProduct = {
		  	productName: pName,
				description: desc,
				price: price,
				supplier: supp,
			  	prodCategory: prodC,
			  	cog: cog,
			  	photo: photo
		  }

		  return Product.findByIdAndUpdate(id, updatedProduct).then((prodUpdated, err) => {
	        
	        if (err) {
	          return false;
	        } else {
	          return  true;
	        }

	    });
		}  
	// Updating inventory count after physical inventory
		module.exports.updateInventCount = (id, details) => {
			let newInventory = details.inventCount;

			let newInventoryCount = {
				inventCount: newInventory
			}

		  return Product.findByIdAndUpdate(id, newInventoryCount).then((prodUpdated, err) => {
	        
	        if (err) {
	          return true;
	        } else {
	          return  false;
	        }

	    });
		}
	// Adding new products' stocks
		module.exports.addInventCount = (id, details) => {

			return Product.findById(id).then(result => {				
					
				let oldInven = result.inventCount
				let newStock = details.inventCount;
				
				let newInven= {
					inventCount: oldInven + newStock
				}

			return Product.findByIdAndUpdate(id, newInven).then((adminStat, err) => {
	        
			    	if (adminStat) {
			          return true;
			        	
			        } else {
			          return false;
			        }
	    		})
			})		
		}
	// Subtracting from inventory count due to error or damage item
		module.exports.diffInventCount = (id, details) => {

			return Product.findById(id).then(result => {				
					
				let oldInven = result.inventCount
				let damageProdNum = details.inventCount;
				
				let newInven= {
					inventCount: oldInven - damageProdNum
				}

			return Product.findByIdAndUpdate(id, newInven).then((adminStat, err) => {
	        
			    		if (adminStat) {
			          return true 
			          // `The product's inventory count with id: ${id} has been updated.`
			        	
			        } else {
			          return false 
			          // `Failed to update inventory count for product with id: ${id}.`
			        }
	    		})
			})		
		}

// [SECTION]: FUNCTIONALITY - DELETE
	// delete product, admin only
		module.exports.deleteProduct = (id) =>{

	    return Product.findByIdAndRemove(id).then((removedProduct, err)=>{
	     
	      if (removedProduct) {
	        return true 
	        // 'Product deletion error';
	      } else {
	        return false 
	        // `Product has been deleted`;
	      }

	    });
	  }
